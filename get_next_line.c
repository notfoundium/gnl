#include <unistd.h>

int		ft_getchar()
{
	char	c;
	char	result;

	if((result = read(0, &c, 1)) > 0)
		return (c);
	if (result == 0)
		return (-1);
	return (-2);
}

char	*ft_resize(char	*old, int *size)
{
	char	*new_str;
	int		i;

	i = 0;
	*size = *size * 2 + 1;
	new_str = (char*)malloc(*size);
	while (i < *size)
		new_str[i++] = 0;
	i = 0;
	while (old[i] != 0)
	{
		new_str[i] = old[i];
		i++;
	}
	free(old);
	return (new_str);
}

int		get_next_line(char **line)
{
	int		current;
	char	*readed;
	int		index;
	int		size;

	index = 0;
	size = 1;
	readed = (char*)malloc(size);
	*readed = 0;
	current = ft_getchar();
	while (current >= 0 && current != '\n')
	{
		if (index == size - 1)
			readed = ft_resize(readed, &size);
		readed[index] = current;
		index++;
		current = ft_getchar();
	}
	*line = readed;
	if (current == '\n')
		return (1);
	if (current == -1)
		return (0);
	return (-1);
}